package com.dev.eventbus;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;

import java.time.LocalTime;
import java.util.UUID;

import static com.dev.eventbus.Constants.TOPIC_NAME;

public class UserVerticle extends AbstractVerticle {
  private String uuid = null;
  private EventBus eventBus = null;

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    this.setUserId()
      .compose(this::setEventBus)
      .compose(this::getMessage)
      .compose(this::sendMessage)
      .onComplete(handler -> {
        if (handler.succeeded()) {
          System.out.println("User Verticle Created Successfully...");
          startPromise.complete();
        } else {
          System.out.println("User Verticle Creation Failed...");
          startPromise.fail(handler.cause());
        }
      });
  }

  public Future<Void> setUserId() {
    this.uuid = UUID.randomUUID().toString();
    System.out.println("UserId assigned: " + uuid);
    return Future.future(f -> f.complete());
  }

  public Future<EventBus> setEventBus(Void v) {
    eventBus = this.vertx.eventBus();
    System.out.println("EventBus initialized... ");

    return Future.future(p -> p.complete(eventBus));
  }

  public Future<EventBus> getMessage(EventBus eventBus) {
    eventBus.consumer(TOPIC_NAME, handle -> {
      System.out.println("@" + uuid + ">> Received Message : " + handle.body().toString());
    });
    return Future.future(f -> f.complete(eventBus));
  }

  public Future<Void> sendMessage(EventBus eventBus) {
    String msg = "New Message ".concat(LocalTime.now().toString());
    JsonObject jo = new JsonObject();
    jo.put("Sender", uuid);
    jo.put("Message", msg);

    System.out.println("[User] send message: " + jo.toString());
    eventBus.send("SERVER", jo.toString());
    return Future.future(f -> f.complete());
  }
}
