package com.dev.eventbus;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;

import static com.dev.eventbus.Constants.TOPIC_NAME;

public class ServerVerticle extends AbstractVerticle {
  private EventBus eventBus = null;

  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    this.eventBus = this.vertx.eventBus();
    this.receiveMessage()
      .onComplete(handler -> {
        if (handler.succeeded()) {
          System.out.println("Server Verticle Created Successfully...");
          startPromise.complete();
        } else {
          System.out.println("Server Verticle Creation Failed...");
          startPromise.fail(handler.cause());
        }
      });
  }

  public Future<Void> receiveMessage() {
    MessageConsumer msgConsumer = this.eventBus.consumer("SERVER", handler -> {
      this.sendMessage(handler);
    });

    System.out.println("[Server] Message Consumer Initialized");

    return Future.future(p -> p.complete());
  }

  public void sendMessage(Message messageHandler) {
    System.out.println("[Server] Received message: " + messageHandler.body().toString());
    String message = messageHandler.body().toString();
    eventBus.publish(TOPIC_NAME, message);
  }
}
